# Minimal Tanken

5 Freunde machen eine lange Urlaubsreise quer durch Europa mit einem Camper. Während der langen Fahrt müssen sie immer wieder halten, um zu tanken.

Justin sagt: "Lasst uns darauf achten, nicht so teuer zu tanken." Esra dagegen möchte möglichst schnell ankommen und meint: "Wir sind lange unterwegs. Ich möchte daher nicht oft anhalten; so groß sind die Preisunterschiede beim Diesel nun auch wieder nicht. Lass uns jedes Mal volltanken, dann kommen wir schneller an."

Nach einiger Diskussion macht Julia den Vorschlag: "Wir machen es so, Wir tanken nur so oft, wie es unbedingt nötig ist; dafür suchen wir uns aber die günstigsten Tankstellen raus."

Vincent hat in der Zwischenzeit mittels einer App auf seinem Smartphone alle auf der Reiseroute liegenden Tankstellen mit ihren Entfernungen und Preisen für Diesel herausgefunden.

Schreibe ein Programm, das den Verbrauch, die Tankgröße, die anfängliche Füllung des Tanks , die Länge der zu fahrenden Strecke und eine Liste von Tankstellen mit jeweiligen Entfernungen und Preisen einliest und berechnet, an welchen Tankstellen getankt werden soll und jeweils wie viel Diesel.

Es muss unbedingt so selten wie möglich getankt werden, selbst wenn häufigeres Tanken billiger sein könnte. Die Anzahl der Tankvorgänge muss also minimal sein, und sie sollen so geplant werden, dass insgesamt möglichst wenig Geld ausgegeben wird. Der Tank darf am Ziel leer sein.

Erstelle für alle Beispieldateien einen Unittest.